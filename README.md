# Simplize

Simplize - Trợ thủ đắc lực giúp nhà đầu tư quản trị danh mục, theo dõi và phân tích mọi cổ phiếu

- Địa chỉ: Số 5 ngõ 316 Lê Trọng Tấn, Thanh Xuân, Hà Nội.

- SDT: 0388408668

Chỉ số Market Cycle Index giúp phân bổ vốn hợp lý vào thị phần chứng khoán Việt Nam. Bạn nên nắm giữ tỷ trọng tiền mặt to và tránh tiêu dùng margin trong thị trường Downturn. ngoài ra, công đoạn Recovery là thời kỳ phù hợp cho các nhà đầu cơ dài hạn.

Chỉ số Market Structure Index giúp cảnh báo sớm những sự kiện khủng hoảng bất thường trong ngắn hạn (cụ thể, khi chỉ số này vượt lên trên mức 100). Bạn nên giảm tỷ trọng cổ phiếu trong ngắn hạn lúc những sự kiện này xảy ra.

https://simplize.vn/

https://www.behance.net/simplize

https://www.flickr.com/people/198639258@N08/

https://vi.gravatar.com/simplizevn
